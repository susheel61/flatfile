import FlatfileImporter from "flatfile-csv-importer";
import $ from "jquery";

const robotImporter = new FlatfileImporter("3512afb9-4d7a-4c8a-b2bc-c5f4385661cc", {
  fields: [
    {
      label: "Robot Name",
      key: "name",
      isRequired: true,
      description: "The designation of the robot",
      validators: [
        {
          validate: "required_without",
          fields: ["id", "shield-color"],
          error: "must be present if no id or shield color"
        }
      ]
    },
    {
      label: "Shield Color",
      key: "shield-color",
      description: "Chromatic value",
      validator: {
        validate: "regex_matches",
        regex: /^[a-zA-Z]+$/,
        error: "Not alphabet only"
      }
    },
    {
      label: "Robot Helmet Style",
      key: "helmet-style",
      type: "select",
      options: ["square", "round", "triangle"]
    },
    {
      label: "Active",
      key: "available",
      type: "checkbox"
    },
    {
      label: "Call Sign",
      key: "sign",
      alternates: ["nickname", "wave"],
      validators: [
        {
          validate: "regex_matches",
          regex: /^[a-zA-Z]{4}$/,
          error: "must be 4 characters exactly"
        },
        {
          validate: "regex_excludes",
          regex: /test/,
          error: 'must not include the word "test"'
        }
      ],
      isRequired: true
    },
    {
      label: "Robot ID Code",
      key: "id",
      description: "Digital identity",
      validators: [
        {
          validate: "regex_matches",
          regex: "numeric",
          error: "must be numeric"
        },
        {
          validate: "required_without",
          fields: ["name"],
          error: "ID must be present if name is absent"
        }
      ]
    }
  ],
  type: "Robot",
  allowInvalidSubmit: true,
  managed: true,
  allowCustom: true,
  disableManualInput: false
});

$("#launch").click(function() {
  robotImporter.setCustomer({ userId: 1, email: "me@david.gs" });
  robotImporter
    .requestDataFromUser()
    .then(function(results) {
      robotImporter.displaySuccess();
      $("#raw_output").text(JSON.stringify(results.data, " ", 2));
    })
    .catch(function(error) {
      console.info(error || "window close");
    });
});
