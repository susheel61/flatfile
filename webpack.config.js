const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = {
    mode: "production",
    entry: {
        "flatfile":"./index.js",
    },
    output: {
        path: path.resolve(__dirname, "dist"),
        filename: "[name].bundle.js"
    },
    devServer: {
        contentBase: path.join(__dirname, "dist"),
        compress: true,
        port: 9000
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader"
                }
            },
            {
                //Applying rules on SCSS or SASS
                test: /\.css$/i,
                //Loaders are applied from right to left.
                use: [
                    MiniCssExtractPlugin.loader, //Used to output css to a seperate file
                    "css-loader",
                    {
                        loader: "postcss-loader", //Run post css actions
                        options: {
                            plugins: function() {
                                // post css plugins, can be exported to postcss.config.js
                                return [
                                    require("precss"),
                                    require("autoprefixer")
                                ];
                            }
                        }
                    },
                    "sass-loader"
                ]
            }
        ]
    },
    plugins: [
        new CleanWebpackPlugin(),
        new MiniCssExtractPlugin({
            filename: "strayer.bundled.css"
        }),
        new HtmlWebpackPlugin({
            template: "index.html",
            meta: {
                viewport:"width=device-width, initial-scale=1, shrink-to-fit=no"
            },
            inject: "body"
        })
    ]
};